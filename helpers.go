package mqtt

import (
	"fmt"
	"math"
	"time"
)

// ExtractValue returns the value of the “value” field in the given payload,
// typically send to the updates channel by a handler created with GetHandler.
// If there is also a “time” field containing a UNIX epoch milliseconds
// timestamp, it is returned as well.  Otherwise, the returned timestamp is
// zero.  This makes it easy to deal with zwave-js-like messages.
func ExtractValue(payload any) (value float64, timestamp time.Time, err error) {
	var ok bool
	var raw map[string]any
	if raw, ok = payload.(map[string]any); !ok {
		err = fmt.Errorf("Message is not a map[string]any but %T: %s", payload, payload)
		return
	} else {
		if rawValue, ok := raw["value"]; !ok {
			err = fmt.Errorf("No “value” field in message: %s", payload)
			return
		} else {
			if value, ok = rawValue.(float64); !ok {
				err = fmt.Errorf("“value” field is not a number: %s", payload)
				return
			} else {
				if timeRaw, ok := raw["time"].(float64); ok {
					if float64(math.MinInt64) < timeRaw && timeRaw < float64(math.MaxInt64) &&
						math.Trunc(timeRaw) == timeRaw {
						timestamp = time.UnixMilli(int64(timeRaw))
					} else {
						err = fmt.Errorf("“time” field is not an int64: %s", payload)
						return
					}
				}
				return
			}
		}
	}
}
