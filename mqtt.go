package mqtt

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"maps"
	"math"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	tbr_logging "gitlab.com/bronger/tools/logging"

	"github.com/eclipse/paho.golang/autopaho"
	"github.com/eclipse/paho.golang/paho"
)

var debug = os.Getenv("DEBUG") == "true"

type client struct {
	cm     *autopaho.ConnectionManager
	Router paho.Router
}

var DefaultClient client

type Options struct {
	QoS    byte
	Retain bool
}

// PublishMsgRawWithOptions sends msg to the given MQTT topic.
func (c client) PublishMsgRawWithOptions(ctx context.Context, logger tbr_logging.Logger,
	topic string, msg []byte, options Options) error {
	if debug {
		logger.Info("Don’t publish MQTT message because in debug mode", "topic", topic, "message", string(msg))
		return nil
	}
	if err := c.cm.AwaitConnection(ctx); err != nil {
		// err can only be the cancelled context
		return err
	}
	pr, err := c.cm.Publish(ctx, &paho.Publish{
		QoS:     options.QoS,
		Retain:  options.Retain,
		Topic:   topic,
		Payload: msg,
	})
	if err != nil {
		return fmt.Errorf("Error publishing to MQTT topic “%s”: %w", topic, err)
	} else if pr.ReasonCode != 0 && pr.ReasonCode != 16 {
		// Reason 16 is that there are no subscribers
		return fmt.Errorf("Error publishing to MQTT topic “%s”: Reason code “%v” received", topic, pr.ReasonCode)
	} else {
		logger.Debug("Sent MQTT message successfully", "message", string(msg), "topic", topic)
		return nil
	}
}

// PublishMsgRawWithOptions sends msg to the given MQTT topic using the default
// client.  See client.PublishMsgRawWithOptions for further information.
func PublishMsgRawWithOptions(ctx context.Context, logger tbr_logging.Logger,
	topic string, msg []byte, options Options) error {
	return DefaultClient.PublishMsgRawWithOptions(ctx, logger, topic, msg, options)
}

// PublishMsgRaw sends msg to the device using MQTT.  The message will not be
// retained, and it has a QoS of 0.  See client.PublishMsgWithOptions for
// further information.
func (c client) PublishMsgRaw(ctx context.Context, logger tbr_logging.Logger, topic string, msg []byte) error {
	return c.PublishMsgRawWithOptions(ctx, logger, topic, msg, Options{})
}

// PublishMsgRaw sends msg to the device using MQTT using the default client.
// See client.PublishMsgRaw for further information.
func PublishMsgRaw(ctx context.Context, logger tbr_logging.Logger, topic string, msg []byte) error {
	return DefaultClient.PublishMsgRaw(ctx, logger, topic, msg)
}

// PublishMsgWithOptions sends “data” to the given MQTT topic using the default
// client.  See client.PublishMsgRawWithOptions for further information.
func PublishMsgWithOptions(ctx context.Context, logger tbr_logging.Logger,
	topic string, data any, options Options) error {
	return DefaultClient.PublishMsgRawWithOptions(ctx, logger, topic, jsonize(data), options)
}

// PublishMsg sends “data” to the device using MQTT using the default client.
// See client.PublishMsgRaw for further information.
func PublishMsg(ctx context.Context, logger tbr_logging.Logger, topic string, data any) error {
	return DefaultClient.PublishMsgRaw(ctx, logger, topic, jsonize(data))
}

// jsonize converts the given data into its JSON serialisation.  If data is a
// map with string keys, a “time” key is added if not already existing.  This
// contains the UNIX timestamp in milliseconds, which is compatible with
// zwave-js.
func jsonize(data any) (msg []byte) {
	if dataAsMap, ok := data.(map[string]any); ok {
		if _, ok := dataAsMap["time"]; !ok {
			dataAsMap["time"] = time.Now().UnixMilli()
		}
	}
	msg, err := json.Marshal(data)
	if err != nil {
		panic(fmt.Errorf("Could not JSON-encode “%v”: %w", data, err))
	}
	return
}

type Update struct {
	Topic      string
	Payload    any
	RawPayload []byte
}

const (
	TypeString = iota
	TypeInt
	TypeFloat
)

// GetHandlerPattern returns a function that can be used for an MQTT router as
// the handler for a topic.  The “updates” channel gets all values from the
// topic.  It works in two modes.
//
// JSON mode: If the pattern starts with “$JSON:”, the incoming MQTT messages
// are interpreted as JSON objects with string keys.  The remainder of the
// pattern after the “$JSON:” is interpreted as a JSON object.  Its keys are
// keys of the MQTT messages, and its values are the keys of the resulting
// Payload mapping in the updates channel.  This way, one can modify this
// mapping to that the code that reads from the updates channel can find its
// values in the Payload mapping.  The “types” parameter is used to validate
// the types of the values in the resulting Payload map.
//
// Also in JSON mode, if isArray is true, the JSON is interpreted as an array
// of object, which in turn are interpreted as above.
//
// Regexp mode: Otherwise, the incoming MQTT messages are interpreted as plain
// strings.  The pattern is then a regular expression with named groups.  The
// Payload in the updates is a map from the group names in the pattern to their
// values found in the MQTT message.  Furthermore, one can pass a “types” map,
// which maps group names to a type (TypeString, TypeInt, or TypeFloat, with
// TypeString being the default).  This determines the data type of the values
// of the Payload map.
//
// In both modes, the Payload mapping is of type map[string]any.  The consumer
// of the updates channel can be sure that the data types of keys contained in
// “types” are correct (in other words, it can typecast without check).  The
// Payload mapping needs not contain all keys mentioned in the regular
// expression group names or the “types” parameters, and it may contain extra
// keys.
//
// If by origin or by JSON remapping at least one of the following keys is
// available and of the correct type, a new key “time.Time” of type [time.Time]
// is added to the update payload, so that the caller can consume this time
// conveniently.
//   - “timestamp”, a UNIX timestamp of type integer
//   - “unixMilli”, a UNIX timestamp in milliseconds of type integer
//   - “rfc3339”, an RFC3339 timestamp of type string
//
// If more than one of them is available, the above is the order of precedence.
//
// The “types” and “isArray” parameters should be hard-coded, while the
// “pattern” parameter typically is user-provided.
func GetHandlerPattern(updates chan<- Update, pattern string, types map[string]int, isArray bool,
	errs chan<- error) (paho.MessageHandler, error) {
	if strings.HasPrefix(pattern, "$JSON:") {
		var nameMapping map[string]string
		jsonString := pattern[len("$JSON:"):]
		if err := json.Unmarshal([]byte(jsonString), &nameMapping); err != nil {
			return nil, fmt.Errorf("Could not parse JSON “%s”: %w", jsonString, err)
		}
		if isArray {
			return func(m *paho.Publish) {
				go func() {
					var payload []map[string]any
					if err := json.Unmarshal(m.Payload, &payload); err != nil {
						errs <- fmt.Errorf("Could not parse JSON message “%s”: %w", m.Payload, err)
						return
					}
					for i, item := range payload {
						if err := enrichAndValidate(item, types, nameMapping); err != nil {
							errs <- fmt.Errorf("Error in item %d in JSON message “%s”: %w",
								i, m.Payload, err)
							return
						}
						convertTimes(item, nameMapping)
					}
					updates <- Update{m.Topic, payload, m.Payload}
				}()
			}, nil
		} else {
			return func(m *paho.Publish) {
				go func() {
					var payload map[string]any
					if err := json.Unmarshal(m.Payload, &payload); err != nil {
						errs <- fmt.Errorf("Could not parse JSON message “%s”: %w", m.Payload, err)
						return
					}
					if err := enrichAndValidate(payload, types, nameMapping); err != nil {
						errs <- fmt.Errorf("Error in JSON message “%s”: %w", m.Payload, err)
						return
					}
					convertTimes(payload, nameMapping)
					updates <- Update{m.Topic, payload, m.Payload}
				}()
			}, nil
		}
	} else {
		if isArray {
			return nil, errors.New("Regular expression patterns not allowed for arrays")
		}
		var (
			re  *regexp.Regexp
			err error
		)
		re, err = regexp.Compile(pattern)
		if err != nil {
			return nil, fmt.Errorf("Could not parse regexp “%s”: %w", pattern, err)
		}
		subexpNames := re.SubexpNames()
		return func(m *paho.Publish) {
			go func() {
				payload := make(map[string]any)
				submatches := re.FindSubmatch(m.Payload)
				for i, name := range subexpNames {
					if name != "" {
						match := string(submatches[i])
						switch types[name] {
						case TypeString:
							payload[name] = match
						case TypeInt:
							if number, err := strconv.Atoi(match); err != nil {
								errs <- fmt.Errorf("Could not parse “%s” as type int for "+
									"group “%s” in topic “%s”: %w", match, name, m.Topic,
									err)
								return
							} else {
								payload[name] = number
							}
						case TypeFloat:
							if number, err := strconv.ParseFloat(match, 64); err != nil {
								errs <- fmt.Errorf("Could not parse “%s” as type float for "+
									"group “%s” in topic “%s”: %w", match, name, m.Topic,
									err)
								return
							} else {
								payload[name] = number
							}
						default:
							panic(fmt.Errorf("Invalid type “%d” for group “%s” in topic “%s”",
								types[name], name, m.Topic))
						}
					}
				}
				convertTimes(payload, nil)
				updates <- Update{m.Topic, payload, m.Payload}
			}()
		}, nil
	}
}

// enrichAndValidate adds new keys as aliases to the given item according to
// the name mapping.  Moreover, it assures that the values of these keys have
// the given types.
func enrichAndValidate(item map[string]any, types map[string]int, nameMapping map[string]string) error {
	for from, to := range nameMapping {
		if value, exists := item[from]; exists {
			item[to] = value
		}
	}
	for name, type_ := range types {
		if value, exists := item[name]; exists {
			var ok bool
			switch type_ {
			case TypeString:
				_, ok = value.(string)
			case TypeInt:
				var floatValue float64
				if floatValue, ok = value.(float64); ok {
					const maxFloatAsInt = 1 << 53
					if ok = -maxFloatAsInt <= floatValue && floatValue <= maxFloatAsInt &&
						math.MinInt <= floatValue && floatValue <= math.MaxInt; ok {
						item[name] = int(floatValue)
					}
				}
			case TypeFloat:
				_, ok = value.(float64)
			default:
				panic(fmt.Errorf("Invalid type “%d” for key “%s”", type_, name))
			}
			if !ok {
				return fmt.Errorf("Could not validate type “%d” for key “%s”", type_, name)
			}
		}
	}
	return nil
}

// convertTimes enriches the given map with an additional key “time.Time”, the
// value of which is always a [time.Time].  It takes the time from the other
// keys “timestamp” (UNIX timestamp), “unixMilli” and “rfc3339” (in this order
// of precedence), if they are available and of the correct type.
func convertTimes(item map[string]any, nameMapping map[string]string) {
	keys := make(map[string]bool)
	for key := range maps.Values(nameMapping) {
		keys[key] = true
	}
	if nameMapping == nil || keys["timestamp"] {
		if value, ok := item["timestamp"].(float64); ok {
			item["time.Time"] = time.Unix(int64(value), 0)
			return
		}
	}
	if nameMapping == nil || keys["unixMilli"] {
		if value, ok := item["unixMilli"].(float64); ok {
			item["time.Time"] = time.UnixMilli(int64(value))
			return
		}
	}
	if nameMapping == nil || keys["rfc3339"] {
		if value, ok := item["rfc3339"].(string); ok {
			if timestamp, err := time.Parse(time.RFC3339, value); err == nil {
				item["time.Time"] = timestamp
				return
			}
		}
	}
}

// GetHandler returns a function that can be used for an MQTT router as the
// handler for a topic.  The “updates” channel gets all values from the topic.
// It is assumed that the topic receives JSON messages.  Before being sent to
// “updates”, they are deserialised.
func GetHandler(updates chan<- Update, errs chan<- error) paho.MessageHandler {
	return func(m *paho.Publish) {
		go func() {
			var payload any
			if err := json.Unmarshal(m.Payload, &payload); err != nil {
				errs <- fmt.Errorf("Could not parse message “%s”: %w", m.Payload, err)
				return
			}
			updates <- Update{m.Topic, payload, m.Payload}
		}()
	}
}

// WaitForShutdown blocks until the MQTT connection handler has shutdown
// cleanly.
func (c client) WaitForShutdown() {
	<-c.cm.Done()
}

// WaitForShutdown blocks until the MQTT connection handler of the default
// client has shutdown cleanly.
func WaitForShutdown() {
	DefaultClient.WaitForShutdown()
}

// AwaitConnection will return when the connection comes up or the context is
// cancelled (only returns an error if context is cancelled).
func (c client) AwaitConnection(ctx context.Context) error {
	return c.cm.AwaitConnection(ctx)
}

// AwaitConnection will return when the connection comes up or the context is
// cancelled (only returns an error if context is cancelled).
func AwaitConnection(ctx context.Context) error {
	return DefaultClient.cm.AwaitConnection(ctx)
}

type (
	// Handlers is a convenience type for writing literals of Topics.
	Handlers []paho.MessageHandler
	// Topics needs to be instatiated and filled for calling GetMQTTClient.
	Topics map[string]Handlers
)

// Add adds a handler to a topic.
func (t Topics) Add(topic string, handler paho.MessageHandler) {
	t[topic] = append(t[topic], handler)
}

// GetMQTTClient returns a new MQTT client.  This effectively starts the MQTT
// communication with the broker at the given address and with the given name
// as the client ID.
func GetMQTTClient(ctx context.Context, logger tbr_logging.Logger, address, name string, topics Topics) (c client) {
	mqttURL, err := url.Parse(address)
	if err != nil {
		panic(fmt.Errorf("Failed to parse URL to MQTT broker: %w", err))
	}
	c.Router = paho.NewStandardRouter()
	numberTopics := len(topics)
	cliCfg := autopaho.ClientConfig{
		BrokerUrls:            []*url.URL{mqttURL},
		KeepAlive:             3600 * 18,
		SessionExpiryInterval: 60,
		OnConnectionUp: func(cm *autopaho.ConnectionManager, connAck *paho.Connack) {
			logger.Info("mqtt connection up")
			if numberTopics > 0 {
				logger.Debug("(Re-)subscribe to all topics", "number", numberTopics)
				subscriptions := new(paho.Subscribe)
				subscriptions.Subscriptions = make([]paho.SubscribeOptions, 0, numberTopics)
				for topic := range topics {
					subscriptions.Subscriptions = append(subscriptions.Subscriptions,
						paho.SubscribeOptions{Topic: topic})
				}
				if _, err := cm.Subscribe(ctx, subscriptions); err != nil {
					panic(fmt.Errorf("Error while re-subscribing: %w", err))
				}
			}
		},
		OnConnectError: func(err error) { logger.Warn("Error whilst attempting connection", "error", err) },
		Debug:          NewLogger(logger),
		ClientConfig: paho.ClientConfig{
			ClientID:      name,
			Router:        c.Router,
			OnClientError: func(err error) { logger.Warn("Server requested disconnect", "error", err) },
			OnServerDisconnect: func(d *paho.Disconnect) {
				if d.Properties != nil {
					logger.Info("Server requested disconnect", "reason", d.Properties.ReasonString)
				} else {
					logger.Info("Server requested disconnect", "reason code", d.ReasonCode)
				}
			},
		},
	}
	c.cm, err = autopaho.NewConnection(ctx, cliCfg)
	if err != nil {
		panic(fmt.Errorf("autopaho.NewConnection returned an error (%w).  API change?", err))
	}
	for topic, handlers := range topics {
		for _, handler := range handlers {
			c.Router.RegisterHandler(topic, handler)
		}
	}
	return
}
