package mqtt

import (
	"fmt"

	tbr_logging "gitlab.com/bronger/tools/logging"
)

type pahoLogger struct {
	logger tbr_logging.Logger
}

func NewLogger(logger tbr_logging.Logger) pahoLogger {
	return pahoLogger{logger}
}

func (l pahoLogger) Println(v ...interface{}) {
	l.logger.Debug("AutoPaho message", "content", fmt.Sprintln(v...))
}

func (l pahoLogger) Printf(format string, v ...interface{}) {
	l.logger.Debug("AutoPaho message", "content", fmt.Sprintf(format, v...))
}
