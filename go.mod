module gitlab.com/bronger/mqtt

go 1.23

require (
	github.com/eclipse/paho.golang v0.22.0
	gitlab.com/bronger/tools v0.0.0-20230825105701-52687403a66d
)

require (
	github.com/gorilla/websocket v1.5.3 // indirect
	golang.org/x/net v0.27.0 // indirect
)
